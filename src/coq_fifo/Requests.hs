{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Requests where

import qualified Prelude
import qualified Bank
import qualified Bool
import qualified Datatypes
import qualified Eqtype
import qualified Ssrnat

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

data Request_kind_t =
   RD
 | WR

coq_Request_kind_eqdef :: Request_kind_t -> Request_kind_t ->
                          Datatypes.Coq_bool
coq_Request_kind_eqdef a b =
  case a of {
   RD -> case b of {
          RD -> Datatypes.Coq_true;
          WR -> Datatypes.Coq_false};
   WR -> case b of {
          RD -> Datatypes.Coq_false;
          WR -> Datatypes.Coq_true}}

coq_Request_kind_eqn :: Eqtype.Equality__Coq_axiom Request_kind_t
coq_Request_kind_eqn x y =
  let {b = coq_Request_kind_eqdef x y} in
  case b of {
   Datatypes.Coq_true -> Bool.ReflectT;
   Datatypes.Coq_false -> Bool.ReflectF}

coq_Request_kind_eqMixin :: Eqtype.Equality__Coq_mixin_of Request_kind_t
coq_Request_kind_eqMixin =
  Eqtype.Equality__Mixin coq_Request_kind_eqdef coq_Request_kind_eqn

coq_Request_kind_eqType :: Eqtype.Equality__Coq_type
coq_Request_kind_eqType =
  unsafeCoerce coq_Request_kind_eqMixin

data Request_t =
   Coq_mkReq Eqtype.Equality__Coq_sort Prelude.Int Request_kind_t Bank.Bank_t 
 Bank.Row_t

coq_Requestor :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                 Request_t -> Eqtype.Equality__Coq_sort
coq_Requestor _ _ r =
  case r of {
   Coq_mkReq requestor _ _ _ _ -> requestor}

coq_Date :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
            Request_t -> Prelude.Int
coq_Date _ _ r =
  case r of {
   Coq_mkReq _ date _ _ _ -> date}

coq_Kind :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
            Request_t -> Request_kind_t
coq_Kind _ _ r =
  case r of {
   Coq_mkReq _ _ kind _ _ -> kind}

coq_Bank :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
            Request_t -> Bank.Bank_t
coq_Bank _ _ r =
  case r of {
   Coq_mkReq _ _ _ bank _ -> bank}

coq_Row :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
           Request_t -> Bank.Row_t
coq_Row _ _ r =
  case r of {
   Coq_mkReq _ _ _ _ row -> row}

coq_Request_eqdef :: Bank.Requestor_configuration -> Bank.Bank_configuration
                     -> Request_t -> Request_t -> Datatypes.Coq_bool
coq_Request_eqdef rEQESTOR_CFG bANK_CFG a b =
  Datatypes.andb
    (Datatypes.andb
      (Datatypes.andb
        (Datatypes.andb
          (Eqtype.eq_op (Bank.coq_Requestor_t rEQESTOR_CFG)
            (coq_Requestor rEQESTOR_CFG bANK_CFG a)
            (coq_Requestor rEQESTOR_CFG bANK_CFG b))
          (Eqtype.eq_op Ssrnat.nat_eqType
            (unsafeCoerce coq_Date rEQESTOR_CFG bANK_CFG a)
            (unsafeCoerce coq_Date rEQESTOR_CFG bANK_CFG b)))
        (Eqtype.eq_op coq_Request_kind_eqType
          (unsafeCoerce coq_Kind rEQESTOR_CFG bANK_CFG a)
          (unsafeCoerce coq_Kind rEQESTOR_CFG bANK_CFG b)))
      (Eqtype.eq_op
        (Eqtype.sig_eqType Ssrnat.nat_eqType (\a0 ->
          Ssrnat.leq (Prelude.succ (unsafeCoerce a0))
            (Bank.coq_BANKS bANK_CFG)))
        (unsafeCoerce coq_Bank rEQESTOR_CFG bANK_CFG a)
        (unsafeCoerce coq_Bank rEQESTOR_CFG bANK_CFG b)))
    (Eqtype.eq_op Ssrnat.nat_eqType
      (unsafeCoerce coq_Row rEQESTOR_CFG bANK_CFG a)
      (unsafeCoerce coq_Row rEQESTOR_CFG bANK_CFG b))

coq_Request_eqn :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                   Eqtype.Equality__Coq_axiom Request_t
coq_Request_eqn rEQESTOR_CFG bANK_CFG x y =
  let {b = coq_Request_eqdef rEQESTOR_CFG bANK_CFG x y} in
  case b of {
   Datatypes.Coq_true -> Bool.ReflectT;
   Datatypes.Coq_false -> Bool.ReflectF}

coq_Request_eqMixin :: Bank.Requestor_configuration ->
                       Bank.Bank_configuration ->
                       Eqtype.Equality__Coq_mixin_of Request_t
coq_Request_eqMixin rEQESTOR_CFG bANK_CFG =
  Eqtype.Equality__Mixin (coq_Request_eqdef rEQESTOR_CFG bANK_CFG)
    (coq_Request_eqn rEQESTOR_CFG bANK_CFG)

coq_Request_eqType :: Bank.Requestor_configuration -> Bank.Bank_configuration
                      -> Eqtype.Equality__Coq_type
coq_Request_eqType rEQESTOR_CFG bANK_CFG =
  unsafeCoerce coq_Request_eqMixin rEQESTOR_CFG bANK_CFG

type Requests_t = Datatypes.Coq_list Request_t

