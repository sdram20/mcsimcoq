module Datatypes where

import qualified Prelude

data Coq_unit =
   Coq_tt

data Coq_bool =
   Coq_true
 | Coq_false

andb :: Coq_bool -> Coq_bool -> Coq_bool
andb b1 b2 =
  case b1 of {
   Coq_true -> b2;
   Coq_false -> Coq_false}

data Coq_list a =
   Coq_nil
 | Coq_cons a (Coq_list a)

