{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Ssrnat where

import qualified Prelude
import qualified Datatypes
import qualified Nat
import qualified Eqtype
import qualified Ssrbool

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

eqn :: Prelude.Int -> Prelude.Int -> Datatypes.Coq_bool
eqn m n =
  (\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))
    (\_ ->
    (\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))
      (\_ -> Datatypes.Coq_true)
      (\_ -> Datatypes.Coq_false)
      n)
    (\m' ->
    (\fO fS n -> if n Prelude.== 0 then fO () else fS (n Prelude.- 1))
      (\_ -> Datatypes.Coq_false)
      (\n' -> eqn m' n')
      n)
    m

eqnP :: Eqtype.Equality__Coq_axiom Prelude.Int
eqnP n m =
  Ssrbool.iffP (eqn n m) (Ssrbool.idP (eqn n m))

nat_eqMixin :: Eqtype.Equality__Coq_mixin_of Prelude.Int
nat_eqMixin =
  Eqtype.Equality__Mixin eqn eqnP

nat_eqType :: Eqtype.Equality__Coq_type
nat_eqType =
  unsafeCoerce nat_eqMixin

addn_rec :: Prelude.Int -> Prelude.Int -> Prelude.Int
addn_rec =
  (Prelude.+)

addn :: Prelude.Int -> Prelude.Int -> Prelude.Int
addn =
  addn_rec

subn_rec :: Prelude.Int -> Prelude.Int -> Prelude.Int
subn_rec =
  Nat.sub

subn :: Prelude.Int -> Prelude.Int -> Prelude.Int
subn =
  subn_rec

leq :: Prelude.Int -> Prelude.Int -> Datatypes.Coq_bool
leq m n =
  Eqtype.eq_op nat_eqType (unsafeCoerce subn m n) (unsafeCoerce 0)

