{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module FIFO where

import qualified Prelude
import qualified Arbiter
import qualified Bank
import qualified Commands
import qualified Datatypes
import qualified Nat
import qualified Requests
import qualified Trace
import qualified Eqtype
import qualified Fintype
import qualified Seq
import qualified Ssrnat

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

coq_ACT_date :: Bank.Bank_configuration -> Prelude.Int
coq_ACT_date bANK_CFG =
  Nat.pred (Bank.coq_T_RP bANK_CFG)

coq_CAS_date :: Bank.Bank_configuration -> Prelude.Int
coq_CAS_date bANK_CFG =
  Ssrnat.addn (coq_ACT_date bANK_CFG) (Bank.coq_T_RCD bANK_CFG)

type FIFO_configuration =
  Prelude.Int
  -- singleton inductive, whose constructor was Build_FIFO_configuration
  
coq_WAIT :: Bank.Bank_configuration -> FIFO_configuration -> Prelude.Int
coq_WAIT _ fIFO_configuration =
  fIFO_configuration

type Counter_t = Fintype.Coq_ordinal

data FIFO_state_t =
   IDLE Counter_t Requests.Requests_t
 | RUNNING Counter_t Requests.Requests_t Requests.Request_t

coq_ARBITER_CFG :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
                   FIFO_configuration -> Bank.Arbiter_configuration
coq_ARBITER_CFG _ _ _ =
  Bank.Build_Arbiter_configuration

coq_OCycle0 :: Bank.Bank_configuration -> FIFO_configuration ->
               Fintype.Coq_ordinal
coq_OCycle0 _ _ =
  0

coq_OACT_date :: Bank.Bank_configuration -> FIFO_configuration ->
                 Fintype.Coq_ordinal
coq_OACT_date bANK_CFG _ =
  coq_ACT_date bANK_CFG

coq_OCAS_date :: Bank.Bank_configuration -> FIFO_configuration ->
                 Fintype.Coq_ordinal
coq_OCAS_date bANK_CFG _ =
  coq_CAS_date bANK_CFG

coq_Next_cycle :: Bank.Bank_configuration -> FIFO_configuration -> Counter_t
                  -> Counter_t
coq_Next_cycle bANK_CFG fIFO_CFG c =
  let {
   nextc = Ssrnat.leq (Prelude.succ (Prelude.succ
             (Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c)))
             (coq_WAIT bANK_CFG fIFO_CFG)}
  in
  case nextc of {
   Datatypes.Coq_true -> Prelude.succ
    (Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c);
   Datatypes.Coq_false -> coq_OCycle0 bANK_CFG fIFO_CFG}

coq_Enqueue :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
               Requests.Requests_t -> Requests.Requests_t ->
               Datatypes.Coq_list Requests.Request_t
coq_Enqueue _ _ =
  Seq.cat

coq_Dequeue :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
               Eqtype.Equality__Coq_sort -> Requests.Requests_t ->
               Datatypes.Coq_list Eqtype.Equality__Coq_sort
coq_Dequeue bANK_CFG rEQESTOR_CFG r p =
  Seq.rem (Requests.coq_Request_eqType rEQESTOR_CFG bANK_CFG) r
    (unsafeCoerce p)

coq_Init_state :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
                  FIFO_configuration -> Requests.Requests_t ->
                  Arbiter.Arbiter_state_t
coq_Init_state bANK_CFG rEQESTOR_CFG fIFO_CFG r =
  Arbiter.Coq_mkArbiterState Datatypes.Coq_nil 0
    (unsafeCoerce (IDLE (coq_OCycle0 bANK_CFG fIFO_CFG)
      (coq_Enqueue bANK_CFG rEQESTOR_CFG r Datatypes.Coq_nil)))

coq_Next_state :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
                  FIFO_configuration -> Requests.Requests_t ->
                  Arbiter.Arbiter_state_t -> Arbiter.Arbiter_state_t
coq_Next_state bANK_CFG rEQESTOR_CFG fIFO_CFG r aS =
  let {
   cmds = Arbiter.coq_Arbiter_Commands rEQESTOR_CFG
            (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS}
  in
  let {
   t = Prelude.succ
    (Arbiter.coq_Arbiter_Time rEQESTOR_CFG
      (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS)}
  in
  let {
   s = Arbiter.coq_Arbiter_State rEQESTOR_CFG
         (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS}
  in
  case unsafeCoerce s of {
   IDLE c p ->
    let {c' = coq_Next_cycle bANK_CFG fIFO_CFG c} in
    let {p' = coq_Enqueue bANK_CFG rEQESTOR_CFG p r} in
    case p of {
     Datatypes.Coq_nil -> Arbiter.Coq_mkArbiterState cmds t
      (unsafeCoerce (IDLE c' p'));
     Datatypes.Coq_cons r0 _ -> Arbiter.Coq_mkArbiterState
      (Datatypes.Coq_cons
      (Commands.coq_PRE_of_req rEQESTOR_CFG bANK_CFG r0 t) cmds) t
      (unsafeCoerce (RUNNING (coq_OCycle0 bANK_CFG fIFO_CFG)
        (unsafeCoerce coq_Dequeue bANK_CFG rEQESTOR_CFG r0 p') r0))};
   RUNNING c p r0 ->
    let {p' = coq_Enqueue bANK_CFG rEQESTOR_CFG p r} in
    let {c' = coq_Next_cycle bANK_CFG fIFO_CFG c} in
    case Eqtype.eq_op Ssrnat.nat_eqType
           (unsafeCoerce Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c)
           (unsafeCoerce Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG)
             (coq_OACT_date bANK_CFG fIFO_CFG)) of {
     Datatypes.Coq_true ->
      let {
       cmds' = Datatypes.Coq_cons
        (Commands.coq_ACT_of_req rEQESTOR_CFG bANK_CFG r0 t) cmds}
      in
      Arbiter.Coq_mkArbiterState cmds' t (unsafeCoerce (RUNNING c' p' r0));
     Datatypes.Coq_false ->
      case Eqtype.eq_op Ssrnat.nat_eqType
             (unsafeCoerce Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c)
             (unsafeCoerce Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG)
               (coq_OCAS_date bANK_CFG fIFO_CFG)) of {
       Datatypes.Coq_true ->
        let {
         cmds' = Datatypes.Coq_cons
          (Commands.coq_CAS_of_req rEQESTOR_CFG bANK_CFG r0 t) cmds}
        in
        Arbiter.Coq_mkArbiterState cmds' t (unsafeCoerce (RUNNING c' p' r0));
       Datatypes.Coq_false ->
        case Eqtype.eq_op Ssrnat.nat_eqType
               (unsafeCoerce Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG)
                 c) (unsafeCoerce Nat.pred (coq_WAIT bANK_CFG fIFO_CFG)) of {
         Datatypes.Coq_true ->
          case p of {
           Datatypes.Coq_nil -> Arbiter.Coq_mkArbiterState cmds t
            (unsafeCoerce (IDLE c' p'));
           Datatypes.Coq_cons r1 _ -> Arbiter.Coq_mkArbiterState
            (Datatypes.Coq_cons
            (Commands.coq_PRE_of_req rEQESTOR_CFG bANK_CFG r1 t) cmds) t
            (unsafeCoerce (RUNNING c'
              (unsafeCoerce coq_Dequeue bANK_CFG rEQESTOR_CFG r1 p') r1))};
         Datatypes.Coq_false -> Arbiter.Coq_mkArbiterState cmds t
          (unsafeCoerce (RUNNING c' p' r0))}}}}

coq_FIFO_counter :: Bank.Bank_configuration -> Bank.Requestor_configuration
                    -> FIFO_configuration -> Arbiter.Arbiter_state_t ->
                    Prelude.Int
coq_FIFO_counter bANK_CFG rEQESTOR_CFG fIFO_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State rEQESTOR_CFG
         (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS of {
   IDLE c _ -> Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c;
   RUNNING c _ _ -> Fintype.nat_of_ord (coq_WAIT bANK_CFG fIFO_CFG) c}

coq_ACommand_t :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
                  Commands.Command_t
coq_ACommand_t =
  Prelude.error "AXIOM TO BE REALIZED"

coq_FIFO_arbiter_trace :: Bank.Bank_configuration ->
                          Bank.Requestor_configuration -> FIFO_configuration
                          -> Arbiter.Implementation_t
coq_FIFO_arbiter_trace bANK_CFG rEQESTOR_CFG fIFO_CFG =
  Arbiter.Coq_mkImplementation
    (coq_Init_state bANK_CFG rEQESTOR_CFG fIFO_CFG)
    (coq_Next_state bANK_CFG rEQESTOR_CFG fIFO_CFG)

isRUNNING :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
             FIFO_configuration -> Arbiter.Arbiter_state_t ->
             Datatypes.Coq_bool
isRUNNING bANK_CFG rEQESTOR_CFG fIFO_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State rEQESTOR_CFG
         (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS of {
   IDLE _ _ -> Datatypes.Coq_false;
   RUNNING _ _ _ -> Datatypes.Coq_true}

isIDLE :: Bank.Bank_configuration -> Bank.Requestor_configuration ->
          FIFO_configuration -> Arbiter.Arbiter_state_t -> Datatypes.Coq_bool
isIDLE bANK_CFG rEQESTOR_CFG fIFO_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State rEQESTOR_CFG
         (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aS of {
   IDLE _ _ -> Datatypes.Coq_true;
   RUNNING _ _ _ -> Datatypes.Coq_false}

coq_FIFO_arbitrate :: Bank.Bank_configuration -> Bank.Requestor_configuration
                      -> FIFO_configuration -> Arbiter.Arrival_function_t ->
                      Prelude.Int -> Trace.Trace_t
coq_FIFO_arbitrate bANK_CFG rEQESTOR_CFG fIFO_CFG aF t =
  Trace.Coq_mkTrace
    (Arbiter.coq_Arbiter_Commands rEQESTOR_CFG
      (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG
      (Arbiter.coq_Default_arbitrate rEQESTOR_CFG
        (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aF
        (coq_FIFO_arbiter_trace bANK_CFG rEQESTOR_CFG fIFO_CFG) t))
    (Arbiter.coq_Arbiter_Time rEQESTOR_CFG
      (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG
      (Arbiter.coq_Default_arbitrate rEQESTOR_CFG
        (coq_ARBITER_CFG bANK_CFG rEQESTOR_CFG fIFO_CFG) bANK_CFG aF
        (coq_FIFO_arbiter_trace bANK_CFG rEQESTOR_CFG fIFO_CFG) t))

coq_FIFO_arbiter :: Bank.Bank_configuration -> Bank.Requestor_configuration
                    -> FIFO_configuration -> Arbiter.Arrival_function_t ->
                    Arbiter.Arbiter_t
coq_FIFO_arbiter =
  coq_FIFO_arbitrate

coq_BANK_CFG :: Bank.Bank_configuration
coq_BANK_CFG =
  Bank.Build_Bank_configuration (Prelude.succ 0) (Prelude.succ 0)
    (Prelude.succ 0) (Prelude.succ (Prelude.succ (Prelude.succ 0)))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    0)))))))))))))))))))) (Prelude.succ (Prelude.succ (Prelude.succ 0)))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ 0))))
    (Prelude.succ (Prelude.succ 0)) (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ 0)))) (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ 0)))) (Prelude.succ 0) (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ 0)))))))))) (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ 0))))))))))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ 0))))))))))))

coq_FIFO_CFG :: FIFO_configuration
coq_FIFO_CFG =
  Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    0)))))))))

coq_REQESTOR_CFG :: Bank.Requestor_configuration
coq_REQESTOR_CFG =
  Eqtype.unit_eqType

coq_Req1 :: Requests.Request_t
coq_Req1 =
  Requests.Coq_mkReq (unsafeCoerce Datatypes.Coq_tt) (Prelude.succ 0)
    Requests.RD 0 (Prelude.succ 0)

coq_Req2 :: Requests.Request_t
coq_Req2 =
  Requests.Coq_mkReq (unsafeCoerce Datatypes.Coq_tt) (Prelude.succ
    (Prelude.succ 0)) Requests.RD 0 (Prelude.succ 0)

coq_Input :: Requests.Requests_t
coq_Input =
  Datatypes.Coq_cons coq_Req2 (Datatypes.Coq_cons coq_Req1 Datatypes.Coq_nil)

coq_AF :: Arbiter.Arrival_function_t
coq_AF =
  Arbiter.coq_Default_arrival_function_t coq_REQESTOR_CFG coq_BANK_CFG
    coq_Input

