module Fintype where

import qualified Prelude

type Coq_ordinal =
  Prelude.Int
  -- singleton inductive, whose constructor was Ordinal
  
nat_of_ord :: Prelude.Int -> Coq_ordinal -> Prelude.Int
nat_of_ord _ i =
  i

