{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Eqtype where

import qualified Prelude
import qualified Bool
import qualified Datatypes
import qualified Specif
import qualified Ssrbool

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

#ifdef __GLASGOW_HASKELL__
type Any = GHC.Base.Any
#else
-- HUGS
type Any = ()
#endif

__ :: any
__ = Prelude.error "Logical or arity value used"

type Equality__Coq_axiom t = t -> t -> Bool.Coq_reflect

data Equality__Coq_mixin_of t =
   Equality__Mixin (Ssrbool.Coq_rel t) (Equality__Coq_axiom t)

_Equality__op :: (Equality__Coq_mixin_of a1) -> Ssrbool.Coq_rel a1
_Equality__op m =
  case m of {
   Equality__Mixin op0 _ -> op0}

type Equality__Coq_type =
  Equality__Coq_mixin_of Any
  -- singleton inductive, whose constructor was Pack
  
type Equality__Coq_sort = Any

_Equality__coq_class :: Equality__Coq_type -> Equality__Coq_mixin_of
                        Equality__Coq_sort
_Equality__coq_class cT =
  cT

eq_op :: Equality__Coq_type -> Ssrbool.Coq_rel Equality__Coq_sort
eq_op t =
  _Equality__op (_Equality__coq_class t)

eqP :: Equality__Coq_type -> Equality__Coq_axiom Equality__Coq_sort
eqP t =
  let {_evar_0_ = \_ a -> a} in
  case t of {
   Equality__Mixin x x0 -> _evar_0_ x x0}

unit_eqP :: Equality__Coq_axiom Datatypes.Coq_unit
unit_eqP _ _ =
  Bool.ReflectT

unit_eqMixin :: Equality__Coq_mixin_of Datatypes.Coq_unit
unit_eqMixin =
  Equality__Mixin (\_ _ -> Datatypes.Coq_true) unit_eqP

unit_eqType :: Equality__Coq_type
unit_eqType =
  unsafeCoerce unit_eqMixin

data Coq_subType t =
   SubType (Any -> t) (t -> () -> Any) (() -> (t -> () -> Any) -> Any -> Any)

type Coq_sub_sort t = Any

val :: (Ssrbool.Coq_pred a1) -> (Coq_subType a1) -> (Coq_sub_sort a1) -> a1
val _ s =
  case s of {
   SubType val0 _ _ -> val0}

sig_subType :: (Ssrbool.Coq_pred a1) -> Coq_subType a1
sig_subType _ =
  SubType (unsafeCoerce Specif.proj1_sig) (unsafeCoerce (\x _ -> x))
    (\_ k_S u -> k_S (unsafeCoerce u) __)

inj_eqAxiom :: Equality__Coq_type -> (a1 -> Equality__Coq_sort) ->
               Equality__Coq_axiom a1
inj_eqAxiom eT f x y =
  Ssrbool.iffP (eq_op eT (f x) (f y)) (eqP eT (f x) (f y))

val_eqP :: Equality__Coq_type -> (Ssrbool.Coq_pred Equality__Coq_sort) ->
           (Coq_subType Equality__Coq_sort) -> Equality__Coq_axiom
           (Coq_sub_sort Equality__Coq_sort)
val_eqP t p sT =
  inj_eqAxiom t (val p sT)

sig_eqMixin :: Equality__Coq_type -> (Ssrbool.Coq_pred Equality__Coq_sort) ->
               Equality__Coq_mixin_of Equality__Coq_sort
sig_eqMixin t p =
  Equality__Mixin (\x y -> eq_op t (Specif.proj1_sig x) (Specif.proj1_sig y))
    (unsafeCoerce val_eqP t p (sig_subType p))

sig_eqType :: Equality__Coq_type -> (Ssrbool.Coq_pred Equality__Coq_sort) ->
              Equality__Coq_type
sig_eqType t p =
  unsafeCoerce sig_eqMixin t p

