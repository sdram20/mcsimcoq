module Commands where

import qualified Prelude
import qualified Bank
import qualified Datatypes
import qualified Requests

data Command_kind_t =
   ACT
 | PRE
 | CRD
 | CWR

data Command_t =
   Coq_mkCmd Prelude.Int Command_kind_t Requests.Request_t

coq_Kind_of_req :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                   Requests.Request_t -> Command_kind_t
coq_Kind_of_req rEQESTOR_CFG bANK_CFG req =
  case Requests.coq_Kind rEQESTOR_CFG bANK_CFG req of {
   Requests.RD -> CRD;
   Requests.WR -> CWR}

coq_PRE_of_req :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                  Requests.Request_t -> Prelude.Int -> Command_t
coq_PRE_of_req _ _ req t =
  Coq_mkCmd t PRE req

coq_ACT_of_req :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                  Requests.Request_t -> Prelude.Int -> Command_t
coq_ACT_of_req _ _ req t =
  Coq_mkCmd t ACT req

coq_CAS_of_req :: Bank.Requestor_configuration -> Bank.Bank_configuration ->
                  Requests.Request_t -> Prelude.Int -> Command_t
coq_CAS_of_req rEQESTOR_CFG bANK_CFG req t =
  Coq_mkCmd t (coq_Kind_of_req rEQESTOR_CFG bANK_CFG req) req

type Commands_t = Datatypes.Coq_list Command_t

