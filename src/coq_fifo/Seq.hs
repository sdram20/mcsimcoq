module Seq where

import qualified Prelude
import qualified Datatypes
import qualified Eqtype
import qualified Ssrbool

cat :: (Datatypes.Coq_list a1) -> (Datatypes.Coq_list a1) ->
       Datatypes.Coq_list a1
cat s1 s2 =
  case s1 of {
   Datatypes.Coq_nil -> s2;
   Datatypes.Coq_cons x s1' -> Datatypes.Coq_cons x (cat s1' s2)}

filter :: (Ssrbool.Coq_pred a1) -> (Datatypes.Coq_list a1) ->
          Datatypes.Coq_list a1
filter a s =
  case s of {
   Datatypes.Coq_nil -> Datatypes.Coq_nil;
   Datatypes.Coq_cons x s' ->
    case a x of {
     Datatypes.Coq_true -> Datatypes.Coq_cons x (filter a s');
     Datatypes.Coq_false -> filter a s'}}

rem :: Eqtype.Equality__Coq_type -> Eqtype.Equality__Coq_sort ->
       (Datatypes.Coq_list Eqtype.Equality__Coq_sort) -> Datatypes.Coq_list
       Eqtype.Equality__Coq_sort
rem t x s =
  case s of {
   Datatypes.Coq_nil -> s;
   Datatypes.Coq_cons y t0 ->
    case Eqtype.eq_op t y x of {
     Datatypes.Coq_true -> t0;
     Datatypes.Coq_false -> Datatypes.Coq_cons y (rem t x t0)}}

