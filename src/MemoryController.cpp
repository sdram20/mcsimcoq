#include <fstream>
#include <iostream>
#include <sstream>
#include "MemoryController.h"
#include "MemorySystem.h"

using namespace MCsim;
MemoryController::MemoryController(const string &systemConfigFile, function<void(Request &)> callback) : callback(callback)
{
	// Assign values to parameters based on configuration file
	readConfigFile(systemConfigFile);

	// Initialize flow control
	clockCycle = 1;
	incomingRequest = NULL;
	outgoingData = NULL;
	outgoingCmd = NULL;

	// Initialize statistic info
	// ============ Stats Tracker ===================
	stats.totalRequest = 0;
	stats.openRead = 0;
	stats.closeRead = 0;
	stats.openWrite = 0;
	stats.closeWrite = 0;
	stats.readBytes = 0;
	stats.writeBytes = 0;
	stats.close = 0;
	stats.open = 0;
	stats.closeRequest = true;
	stats.open_counter = 0;
	stats.close_counter = 0;
	stats.openRead_Latency = 0;
	stats.closeRead_Latency = 0;
	stats.openWrite_Latency = 0;
	stats.closeWrite_Latency = 0;
	//memorySystem = NULL;
	memoryDevice = NULL;
}

MemoryController::MemoryController(MemorySystem *ms, const string &systemConfigFile)

{

	// assign values to parameters based on configuration file
	readConfigFile(systemConfigFile);

	parentMemorySystem = ms;
	//Initialize flow control
	clockCycle = 0;
	incomingRequest = NULL;
	outgoingData = NULL;
	outgoingCmd = NULL;

	// Initialize statistic info
	// ============ Stats Tracker ===================
	stats.totalRequest = 0;
	stats.openRead = 0;
	stats.closeRead = 0;
	stats.openWrite = 0;
	stats.closeWrite = 0;
	stats.readBytes = 0;
	stats.writeBytes = 0;
	stats.close = 0;
	stats.open = 0;
	stats.closeRequest = true;
	stats.open_counter = 0;
	stats.close_counter = 0;
	stats.openRead_Latency = 0;
	stats.closeRead_Latency = 0;
	stats.openWrite_Latency = 0;
	stats.closeWrite_Latency = 0;
	memoryDevice = NULL;
}

MemoryController::MemoryController(MemorySystem *ms, const string &systemConfigFile, function<void(Request &)> callback) : callback(callback)

{

	// assign values to parameters based on configuration file
	readConfigFile(systemConfigFile);

	parentMemorySystem = ms;
	//Initialize flow control
	clockCycle = 0;
	incomingRequest = NULL;
	outgoingData = NULL;
	outgoingCmd = NULL;

	// Initialize statistic info
	// ============ Stats Tracker ===================
	stats.totalRequest = 0;
	stats.openRead = 0;
	stats.closeRead = 0;
	stats.openWrite = 0;
	stats.closeWrite = 0;
	stats.readBytes = 0;
	stats.writeBytes = 0;
	stats.close = 0;
	stats.open = 0;
	stats.closeRequest = true;
	stats.open_counter = 0;
	stats.close_counter = 0;
	stats.openRead_Latency = 0;
	stats.closeRead_Latency = 0;
	stats.openWrite_Latency = 0;
	stats.closeWrite_Latency = 0;
	memoryDevice = NULL;
}

MemoryController::~MemoryController() // dtor
{
	// Delete Schedulers
	delete schedulerRegister;
	// Delete Queues
	for (auto it = requestQueue.begin(); it != requestQueue.end(); it++)
	{
		delete (*it);
	}
	requestQueue.clear();
	for (auto it = commandQueue.begin(); it != commandQueue.end(); it++)
	{
		delete (*it);
	}
	commandQueue.clear();
	pendingReadRequest.clear();
	pendingWriteRequest.clear();

	myTrace.close();
}

void MemoryController::setRequestor(unsigned int id, bool criticality)
{
	requestorCriticalTable[id] = criticality;
}

void MemoryController::connectMemoryDevice(MemoryDevice *memDev)
{
	DEBUG("\t [MemoryController::connectMemoryDevice]");
	memoryDevice = memDev;
	memTable[Rank] = memoryDevice->get_Rank();
	memTable[BankGroup] = memoryDevice->get_BankGroup();
	memTable[Bank] = memoryDevice->get_Bank();
	memTable[Row] = memoryDevice->get_Row();
	memTable[Column] = memoryDevice->get_Column();

	dataBusWidth = memoryDevice->get_DataBus();

	//MH: we set requestors criticality based on coreID for now, this can change based on controller type
	// Ideally, this would be a parameter from config file

	unsigned int numberRequestors;
	bool isHRT = false;
	numberRequestors = parentMemorySystem->numberRequestors;
	for (unsigned int id = 0; id < numberRequestors; id++)
	{
		if (id == 0)
			isHRT = true;
		else
			isHRT = true;
		setRequestor(id, isHRT);
	}
	// construct the request and command queues
	unsigned int queueNum = queueNumber(reqMap);
	DEBUG("\t [MemoryController::connectMemoryDevice] Number of reques queues: " << queueNum);
	if (queueNum != 0)
	{
		for (unsigned int index = 0; index < queueNum; index++)
		{
			DEBUG("\t [MemoryController::connectMemoryDevice] requestorReqQ: " << requestorReqQ);
			requestQueue.push_back(new RequestQueue(requestorReqQ, writeQueueEnable));
		}
	}
	queueNum = queueNumber(cmdMap);
	DEBUG("\t [MemoryController::connectMemoryDevice] Number of command queues: " << queueNum);
	if (queueNum != 0)
	{
		for (unsigned int index = 0; index < queueNum; index++)
		{
			commandQueue.push_back(new CommandQueue(requestorCmdQ));
		}
	}
	// Configured Scheduler based on configuration table
	schedulerRegister = new SchedulerRegister(dataBusWidth, requestorCriticalTable, requestQueue, commandQueue);
	addressMapping = new AddressMapping(configTable["AddressMapping"], memTable);
	requestScheduler = schedulerRegister->getRequestScheduler(configTable["RequestScheduler"]);
	commandGenerator = schedulerRegister->getCommandGenerator(configTable["CommandGenerator"]);
	commandScheduler = schedulerRegister->getCommandScheduler(configTable["CommandScheduler"]);
	requestScheduler->connectCommandGenerator(commandGenerator);
	commandScheduler->connectMemoryDevice(memDev);
}

ForeignRequest_t * mcsimreq2coqreq (MCsim::Request * ptr){
	ForeignRequest_t * req = new ForeignRequest_t();
	req->requestType = (CoqRequestType) ptr->requestType;
	req->requestorID = ptr->requestorID;
	req->requestSize = ptr->requestSize;
	req->address = ptr->address;
	req->ddata = ptr->data;
	req->arriveTime = ptr->arriveTime;
	req->returnTime = ptr->returnTime;
	req->rank = ptr->rank;
	req->bankGroup = ptr->bankGroup;
	req->bank = ptr->bank;
	req->subArray = ptr->subArray;
	req->row = ptr->row;
	req->col = ptr->col;
	for (size_t i = 0; i < 4; i++){
		req->addressMap[i] = ptr->addressMap[i];
	}
	return req;
}

ForeignCommand_t * mcsimcmd2coqcmd (MCsim::BusPacket * cmd){
	ForeignCommand_t * coq_cmd = new ForeignCommand_t();
	coq_cmd->busPacketType = (CoqBusPacketType) cmd->busPacketType;
	coq_cmd->requestorID = cmd->requestorID;
	coq_cmd->address = cmd->address;
	coq_cmd->column = cmd->column;
	coq_cmd->row = cmd->row;
	coq_cmd->subArray = cmd->subArray;
	coq_cmd->bank = cmd->bank;
	coq_cmd->bankGroup = cmd->bankGroup;
	coq_cmd->rank = cmd->rank;
	coq_cmd->ddata = cmd->data;
	coq_cmd->arriveTime = cmd->arriveTime;
	coq_cmd->postCommand = cmd->postCommand;
	coq_cmd->postCounter = cmd->postCounter;
	for (size_t i = 0; i < 4; i++){
		coq_cmd->addressMap[i] = cmd->addressMap[i];
	}
	return coq_cmd;
}

MCsim::BusPacket * coqcmd2mcsimcmd(ForeignCommand_t * cmd){
	MCsim::BusPacket * mcsim_cmd = new BusPacket(
		(MCsim::BusPacketType) cmd->busPacketType,
		cmd->requestorID,
		cmd->address,
		cmd->column,
		cmd->row,
		cmd->bank,
		cmd->rank,
		cmd->subArray,
		cmd->ddata,
		cmd->arriveTime
	);
	return mcsim_cmd;
}

bool MemoryController::addRequest(unsigned int requestorID, unsigned long long address, bool R_W, unsigned int size)
{
	// add the request to the list of files I'm sending to Coq
	string type = "write";
	if (R_W)
	{
		type = "read";
	}
	myTrace << clockCycle << "," << type << "," << address << ","
			<< "16," << requestorID << "\n";

	RequestType requestType = DATA_READ;
	if (!R_W)
	{
		requestType = DATA_WRITE;
	}
	incomingRequest = new Request(requestorID, requestType, size, address, NULL);
	incomingRequest->arriveTime = clockCycle;
	addressMapping->addressMapping(incomingRequest); // Here the address mapping will occures
	// ============ Stats Tracker ===================
	if (incomingRequest->requestorID == 0)
	{
		stats.totalRequest++;
	}

	//-------------------------------------------------------------------------------
	// ---------------------------------- Coq stuff ----------------------------------
	// By this point the request has already gone through address maping
	DEBUG("\t\t [MemoryController::addRequest] adding to the coq request queue")
	ForeignRequest_t * req = mcsimreq2coqreq(incomingRequest);
	DEBUG("\t\t [MemoryController::addRequest] requestor id: " << req->requestorID);
	DEBUG("\t\t [MemoryController::addRequest] address: " << req->address);
	DEBUG("\t\t [MemoryController::addRequest] type: " << req->requestType);
	coq_requestQueue.push_back(req);
	coq_pending.push_back(req);
	//-------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------

	// Decode the request queue index
	unsigned int queueIndex = decodeQueue(incomingRequest->addressMap, reqMap); // True indicate requestQueue
	if (requestQueue[queueIndex]->insertRequest(incomingRequest))
	{
		if (incomingRequest->requestType == DATA_READ)
		{
			pendingReadRequest.push_back(incomingRequest); // Pending for return Data
		}
		else
		{
			pendingWriteRequest.push_back(incomingRequest); // Pending for write acknowledgement
		}
	}
	incomingRequest = NULL;
	return true;
}

bool MemoryController::enqueueCommand(BusPacket *command)
{
	DEBUG("\t [enqueueCommand]");
	DEBUG("\t [enqueueCommand] requestorID: " << command->requestorID);
	// ============ Stats Tracker ===================
	if (command->requestorID == 0)
	{
		if (command->busPacketType == ACT_R)
		{
			stats.closeRequest = true;
		}
		else if (command->busPacketType == ACT_W)
		{
			stats.closeRequest = true;
		}
		else if (command->busPacketType == ACT)
		{
			stats.closeRequest = true;
		}
		else if (command->busPacketType < ACT_R)
		{
			if (stats.closeRequest)
			{
				stats.close_counter = 0;
				if (command->busPacketType == RDA || command->busPacketType == RD)
					stats.closeRead++;
				else
					stats.closeWrite++;
			}
			else
			{
				stats.open_counter = 0;
				if (command->busPacketType == RDA || command->busPacketType == RD)
					stats.openRead++;
				else
					stats.openWrite++;
			}
		}
	}
	// DEBUG("\t [enqueueCommand] got here 1");
	command->addressMap[Rank] = 0; //command->rank;
	command->addressMap[Bank] = command->bank;
	command->addressMap[BankGroup] = 0; //command->bankGroup;
	command->addressMap[SubArray] = command->subArray;
	command->arriveTime = clockCycle;
	// DEBUG("\t [enqueueCommand] got here 2");

	unsigned int queueIndex = decodeQueue(command->addressMap, cmdMap); // false indicate commandQueue
	DEBUG("\t [enqueueCommand] queueIndex: " << queueIndex);
	return commandQueue[queueIndex]->insertCommand(command, requestorCriticalTable[command->requestorID]);
}
void MemoryController::flushWrite(bool sw)
{
	if (writeQueueEnable)
		requestScheduler->flushWriteReq(sw);
}

void MemoryController::update()
{
	//-------------------------------------------------------------------------------
	// ---------------------------------- Coq stuff ----------------------------------
	unsigned int num_req = coq_requestQueue.size();
	ForeignRequest_t * reqlist = (ForeignRequest_t *) malloc(num_req * sizeof(ForeignRequest_t));
	
	for (size_t i = 0; i < num_req; i++)
	{
		reqlist[i] = *(coq_requestQueue[i]);
	}

	if(clockCycle == 0) 
	{	
		coq_state = tdm_init_state_wrapper(num_req,reqlist);
	}
	else
	{
		coq_state = tdm_next_state_wrapper(num_req,reqlist,coq_state);
	}
	// print_state(coq_state);
	coq_cmd = (ForeignCommand_t *) getcommand(coq_state);
	ForeignRequest_t * related_req;

	// This represents a NOP
	if((int)coq_cmd->arriveTime == -1){
		outgoingCmd = NULL;
		DEBUG("\t [COQ] Issuing NOP");
	} else {
		DEBUG("\t [COQ] coq_cmd requestor ID: " << coq_cmd->requestorID);
		DEBUG("\t [COQ] coq_cmd column: " << coq_cmd->column);
		related_req = coq_pending.front();
		// busPacketTypes, requestorID, row and bank come from Coq

		// This comes from the associated request
		coq_cmd->address = related_req->address;
		coq_cmd->column = related_req->col;
		coq_cmd->subArray = related_req->subArray;
		coq_cmd->bankGroup = related_req->bankGroup;
		coq_cmd->rank = related_req->rank;

		// These are default values
		coq_cmd->ddata = 0;
		coq_cmd->arriveTime = 0;
		coq_cmd->postCommand = false;
		coq_cmd->postCounter = 0;
		// coq_cmd->addressMap = {1,0,1,0};

		// data is only valid when the command is a CAS
		if(coq_cmd->busPacketType == CoqBusPacketType::RD || coq_cmd->busPacketType == CoqBusPacketType::WR){
			coq_cmd->ddata = related_req->ddata;
			coq_pending.erase(coq_pending.begin());
		}
		outgoingCmd = coqcmd2mcsimcmd(coq_cmd);
	}
	coq_requestQueue.clear();
	free(coq_cmd);

	//requestScheduler->requestSchedule();

	DEBUG("\t [COQ] Removing the command generated by MCsim from the buffer");
	DEBUG("\t [COQ] Command buffer size before replacement:" << commandGenerator->bufferSize());

	// Cancel the thing they are doing - clean the command buffer
	// for (size_t i = 0; i < commandGenerator->bufferSize(); i++)
	// {
	// 	commandGenerator->commandBuffer.pop();
	// }
	// for (size_t i = 0; i < commandGenerator->bufferSize(); i++)
	// {
	// 	commandGenerator->commandBuffer.pop();
	// }
	// for (size_t i = 0; i < commandGenerator->bufferSize(); i++)
	// {
	// 	commandGenerator->commandBuffer.pop();
	// }
	
	// DEBUG("\t Buffer size after:" << commandGenerator->bufferSize());
	// for (size_t i = 0; i < commandGenerator->bufferSize(); i++)
	// {
	// 	DEBUG("\t Buspacket type: " << commandGenerator->commandBuffer.front()->busPacketType);
	// }

	// add my bus packet generated by coq into it
	if(outgoingCmd != NULL){
		commandGenerator->commandBuffer.push(outgoingCmd);
		DEBUG("\t [COQ] Adding the COQ generated command in the buffer");
		DEBUG("\t [COQ] Command buffer size after replacement: " << commandGenerator->bufferSize());
	}
	
	// So far, req queue have been created. Upon this line, a request will be chosen and remove from either general buffer or REQ buffers and pushed into the command buffers
	while (commandGenerator->bufferSize() > 0)
	{
		if (enqueueCommand(commandGenerator->getCommand()))
		{
			commandGenerator->removeCommand();
			DEBUG("\t [void MemoryController::update()] Now removing my command. Buffer size after:" << commandGenerator->bufferSize());
		}
		else
		{
			DEBUG("MEMORY CONTROLLER: COMMAND NOT INSERTED TO THE QUEUE");
			abort();
		}
	}
	commandScheduler->refresh();
	
	outgoingCmd = commandScheduler->commandSchedule();

	if (outgoingCmd != NULL)
	{
		DEBUG("\t\t [COQ] outgoingCmd.busPacketType: " << outgoingCmd->busPacketType);
		DEBUG("\t\t [COQ] outgoingCmd.requestorID: " << outgoingCmd->requestorID);
		DEBUG("\t\t [COQ] outgoingCmd.address: " << outgoingCmd->address);
		DEBUG("\t\t [COQ] outgoingCmd.column: " << outgoingCmd->column);
		DEBUG("\t\t [COQ] outgoingCmd.row: " << outgoingCmd->row);
		DEBUG("\t\t [COQ] outgoingCmd.subArray: " << outgoingCmd->subArray);
		DEBUG("\t\t [COQ] outgoingCmd.bank: " << outgoingCmd->bank);
		DEBUG("\t\t [COQ] outgoingCmd.bankGroup: " << outgoingCmd->bankGroup);
		DEBUG("\t\t [COQ] outgoingCmd.rank: " << outgoingCmd->rank);
		DEBUG("\t\t [COQ] outgoingCmd.data: " << (unsigned int *) (outgoingCmd->data));
		DEBUG("\t\t [COQ] outgoingCmd.arriveTime: " << outgoingCmd->arriveTime);
		DEBUG("\t\t [COQ] outgoingCmd.postCommand: " << outgoingCmd->postCommand);
		DEBUG("\t\t [COQ] outgoingCmd.postCounter: " << outgoingCmd->postCounter);
		DEBUG("\t\t [COQ] outgoingCmd.addressMap: " << *(outgoingCmd->addressMap));
		if (outgoingCmd->requestorID == 0 && outgoingCmd->busPacketType <= ACT)
		{
		}
		// outgoingCmd->rank instead of zero
		if (outgoingCmd->busPacketType == WR || outgoingCmd->busPacketType == WRA)
		{
			sendDataBuffer.push_back(new BusPacket(DATA, outgoingCmd->requestorID, outgoingCmd->address, outgoingCmd->column,
												   outgoingCmd->row, outgoingCmd->bank, 0, outgoingCmd->subArray, outgoingCmd->data, clockCycle));
			if (outgoingCmd->postCommand)
			{
				// additive latency (AL) is tRCD - 1
				sendDataCounter.push_back(memoryDevice->get_constraints("tRCD") - 1 + memoryDevice->get_constraints("tWL") + memoryDevice->get_constraints("tBus"));
			}
			else
			{
				sendDataCounter.push_back(memoryDevice->get_constraints("tWL") + memoryDevice->get_constraints("tBus"));
			}
		}
	}
	delete outgoingCmd;
	outgoingCmd = NULL;

	if (sendDataCounter.size() > 0)
	{
		if (sendDataCounter.front() == 0)
		{
			outgoingData = sendDataBuffer.front();
			sendData(outgoingData);
			delete outgoingData;
			outgoingData = NULL;
			sendDataCounter.erase(sendDataCounter.begin());
			sendDataBuffer.erase(sendDataBuffer.begin());
		}
		for (unsigned int i = 0; i < sendDataCounter.size(); i++)
		{
			sendDataCounter[i]--;
		}
	}
	// step through the scheduler components
	requestScheduler->step();
	commandScheduler->tick();
	clockCycle++;

	// ============ Stats Tracker ===================
	stats.close_counter++;
	stats.open_counter++;
}

void MemoryController::receiveData(BusPacket *bpacket)
{
	DEBUG("\t [MemoryController::receiveData] ")
	stats.readBytes += dataBusWidth;
	// Checking with pending request
	for (unsigned int index = 0; index < pendingReadRequest.size(); index++)
	{
		if (bpacket->requestorID == pendingReadRequest[index]->requestorID &&
			bpacket->address == pendingReadRequest[index]->address)
		{
			if (pendingReadRequest[index]->requestSize == dataBusWidth)
			{
				callback(*pendingReadRequest[index]);
				// ============ Stats Tracker ===================
				if (bpacket->requestorID == 0)
				{
					if (stats.closeRequest)
					{
						if (stats.closeRead_Latency < stats.close_counter)
						{
							stats.closeRead_Latency = stats.close_counter;
						}
						stats.closeRequest = false;
					}
					else
					{
						if (stats.openRead_Latency < stats.open_counter)
						{
							stats.openRead_Latency = stats.open_counter;
						}
					}
					stats.close_counter = 0; // works for in order
					stats.open_counter = 0;
				}
				// *** Deallocation
				delete pendingReadRequest[index];
				pendingReadRequest.erase(pendingReadRequest.begin() + index);
			}
			else
			{
				pendingReadRequest[index]->requestSize -= dataBusWidth;
			}
			break;
		}
	}
}

void MemoryController::sendData(BusPacket *bpacket)
{
	memoryDevice->receiveFromBus(bpacket);
	stats.writeBytes += dataBusWidth;

	for (unsigned int index = 0; index < pendingWriteRequest.size(); index++)
	{
		if (bpacket->requestorID == pendingWriteRequest[index]->requestorID &&
			bpacket->address == pendingWriteRequest[index]->address)
		{
			if (pendingWriteRequest[index]->requestSize == dataBusWidth)
			{
				callback(*pendingWriteRequest[index]);

				// ============ Stats Tracker ===================
				if (bpacket->requestorID == 0)
				{
					if (stats.closeRequest)
					{
						if (stats.closeWrite_Latency < stats.close_counter)
						{
							stats.closeWrite_Latency = stats.close_counter;
						}
						stats.closeRequest = false;
					}
					else
					{
						if (stats.openWrite_Latency < stats.open_counter)
						{
							stats.openWrite_Latency = stats.open_counter;
						}
					}
					stats.close_counter = 0; // works for in order
					stats.open_counter = 0;
				}

				// **** Deallocation
				delete pendingWriteRequest[index];
				pendingWriteRequest.erase(pendingWriteRequest.begin() + index);
			}
			else
			{
				pendingWriteRequest[index]->requestSize -= dataBusWidth;
			}
			break;
		}
	}
}
void MemoryController::returnReadData(const Request *req)
{
	if (parentMemorySystem->ReturnReadData != NULL)
	{
		(*parentMemorySystem->ReturnReadData)(parentMemorySystem->systemID, req->address, clockCycle);
	}
}

void MemoryController::writeDataDone(const Request *req)
{
	if (parentMemorySystem->WriteDataDone != NULL)
	{
		(*parentMemorySystem->WriteDataDone)(parentMemorySystem->systemID, req->address, clockCycle);
	}
}

unsigned int MemoryController::generalBufferSize()
{
	return requestQueue[0]->generalReadBufferSize(false);
}

bool MemoryController::isWriteModeFromController()
{
	return requestQueue[0]->isWriteMode();
}
void MemoryController::trackError(int id)
{
	DEBUG("REQUEST QUEUE: " << requestQueue[id]->getSize(true, 0) << " COMMAND QUEUE: " << commandQueue[id]->getSize(true));
}

void MemoryController::printResult()
{
	PRINT("\n");
	PRINT("Statistics Assuming Cores are In Order:");
	PRINT("\n");
	PRINT("REQ under analysis: Open Request Ratio : " << (float)(stats.openRead + stats.openWrite) / (stats.openRead + stats.openWrite + stats.closeRead + stats.closeWrite));
	PRINT("   ");
	PRINT("REQ under analysis: WC Open Read : " << stats.openRead_Latency << "	"
												<< "WC Close Read : " << stats.closeRead_Latency << "	"
												<< "WC Open Write : " << stats.openWrite_Latency << "	"
												<< "WC Close Write : " << stats.closeWrite_Latency);
	PRINT("   ");
	PRINT("-----------------------------------------Simulation Summary---------------------------------------------");
	PRINT("Bandwidth = " << (float)(stats.readBytes + stats.writeBytes) / (clockCycle * memoryDevice->get_constraints("tCK")) * 1000 << " MB/s");
	PRINT("   ");
	PRINT("Number of Open Read     :  " << stats.openRead);
	PRINT("Number of Close Read	:  " << stats.closeRead);
	PRINT("Number of Open Write    :  " << stats.openWrite);
	PRINT("Number of Close Write   :  " << stats.closeWrite);
	PRINT("   ");
}

void MemoryController::displayConfiguration()
{
	PRINT("---------------------------------------Configuration Summary--------------------------------------------");
	PRINT("Rank count   :   " << memTable[Rank]);
	PRINT("Bank count   :   " << memTable[Bank]);
	PRINT("Row count    :   " << memTable[Row]);
	PRINT("Column count :   " << memTable[Column]);
	PRINT("	Address Mapping 	: " << configTable["AddressMapping"]);
	PRINT("	Request Queue 		: " << requestQueue.size() << " perREQ " << requestorReqQ << " WBuffer " << writeQueueEnable);
	PRINT("	Request Scheduler 	: " << configTable["RequestScheduler"]);
	PRINT("	Command Generator 	: " << configTable["CommandGenerator"]);
	PRINT("	Command Queue 		: " << commandQueue.size() << " perREQ " << requestorCmdQ);
	PRINT("	Command Scheduler 	: " << configTable["CommandScheduler"]);
}

// Read Value from Configuration File
void MemoryController::readConfigFile(const string &filename)
{
	std::ifstream configFile;
	string line, key, valueString;
	size_t commentIndex, equalsIndex;

	configFile.open(filename.c_str());
	size_t lineNumber = 0;
	if (configFile.is_open())
	{
		while (!configFile.eof())
		{
			lineNumber++;
			getline(configFile, line);
			// Skip zero-length lines
			if (line.size() == 0)
			{
				continue;
			}
			// Search for a comment char
			if ((commentIndex = line.find_first_of(";")) != string::npos)
			{
				// If the comment char is the first char, ignore the whole line
				if (commentIndex == 0)
				{
					continue;
				}
				// Truncate the line at first comment before going on
				line = line.substr(0, commentIndex);
			}
			// Trim off the end spaces that might have been between the value and comment char
			size_t whiteSpaceEndIndex;
			if ((whiteSpaceEndIndex = line.find_last_not_of(" \t")) != string::npos)
			{
				line = line.substr(0, whiteSpaceEndIndex + 1);
			}
			// A line has to have an equals sign
			if ((equalsIndex = line.find_first_of("=")) == string::npos)
			{
				ERROR("Malformed Line " << lineNumber << " (missing equals)");
				abort();
			}
			size_t strlen = line.size();
			// All characters before the equals are the key
			key = line.substr(0, equalsIndex);
			// All characters after the equals are the value
			valueString = line.substr(equalsIndex + 1, strlen - equalsIndex);
			SetKey(key, valueString);
		}
	}
	configFile.close();
}

void MemoryController::SetKey(string key, string valueString)
{
	// Request Queue Structure (if perRequestor or writeBuffer enabled)
	if (key == "RequestQueue")
	{
		setQueue(valueString, reqMap);
	}
	else if (key == "WriteQueue")
	{
		writeQueueEnable = convertNumber(valueString);
	} // 0 is false, 1 is true
	else if (key == "ReqPerREQ")
	{
		requestorReqQ = convertNumber(valueString);
	}
	// Command Queue Structure (if perRequestor enabled)
	else if (key == "CommandQueue")
	{
		setQueue(valueString, cmdMap);
	}
	else if (key == "CmdPerREQ")
	{
		requestorCmdQ = convertNumber(valueString);
	}
	// Memory Controller Scheduler Identification
	else
	{
		configTable[key] = valueString;
	}
}

unsigned int MemoryController::convertNumber(const std::string &input)
{
	unsigned int intValue = 0;
	std::istringstream ss(input);
	if (isNumeric(input))
	{
		ss >> intValue;
	}
	else
	{
		cout << "Wrong Operation" << endl;
	}
	return intValue;
}

bool MemoryController::isNumeric(const std::string &input)
{
	return std::all_of(input.begin(), input.end(), ::isdigit);
}

void MemoryController::setQueue(const std::string &queueOrg, vector<unsigned int> &queueMap)
{
	for (unsigned index = 0; index < queueOrg.size(); index++)
	{
		if (queueOrg[index] == '1')
		{
			queueMap.push_back(1);
		}
		else
		{
			queueMap.push_back(0);
		}
	}
}

unsigned int MemoryController::queueNumber(vector<unsigned int> &queueMap)
{
	unsigned int queueCount = 1;
	for (unsigned int index = 0; index < queueMap.size(); index++)
	{
		if (queueMap[index] == 1)
		{
			for (unsigned int n = 0; n <= index; n++)
			{
				queueCount = queueCount * memTable[n]; // Each sub-level is a multiple of upper level
			}
			break;
		}
	}
	return queueCount;
}

unsigned int MemoryController::decodeQueue(const unsigned int (&addressMap)[4], vector<unsigned int> &queueMap)
{
	unsigned int queueIndex = 0;
	for (unsigned int index = 0; index < queueMap.size(); index++)
	{
		if (queueMap[index] == 1)
		{
			queueIndex = addressMap[index];
			unsigned int level = 0;
			for (unsigned int n = 0; n < index; n++)
			{
				if (addressMap[n] > 0)
				{
					if (level == 0)
					{
						level = addressMap[n];
					}
					else
					{
						level = level * addressMap[n];
					}
				}
			}
			queueIndex = queueIndex + level * memTable[index];
			break;
		}
	}
	return queueIndex;
}
