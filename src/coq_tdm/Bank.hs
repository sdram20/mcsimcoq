{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Bank where

import qualified Prelude
import qualified Eqtype

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
type Any = GHC.Base.Any
#else
-- HUGS
type Any = ()
#endif

type Row_t = Prelude.Int

type Requestor_configuration =
  Eqtype.Equality__Coq_type
  -- singleton inductive, whose constructor was Build_Requestor_configuration
  
coq_Requestor_t :: Requestor_configuration -> Eqtype.Equality__Coq_type
coq_Requestor_t requestor_configuration =
  requestor_configuration

data Arbiter_configuration =
   Build_Arbiter_configuration

type State_t = Any

data Bank_configuration =
   Build_Bank_configuration Prelude.Int Prelude.Int Prelude.Int Prelude.Int 
 Prelude.Int Prelude.Int Prelude.Int Prelude.Int Prelude.Int Prelude.Int 
 Prelude.Int Prelude.Int Prelude.Int Prelude.Int

coq_BANKS :: Bank_configuration -> Prelude.Int
coq_BANKS bank_configuration =
  case bank_configuration of {
   Build_Bank_configuration bANKS _ _ _ _ _ _ _ _ _ _ _ _ _ -> bANKS}

coq_T_RP :: Bank_configuration -> Prelude.Int
coq_T_RP bank_configuration =
  case bank_configuration of {
   Build_Bank_configuration _ _ _ _ _ _ t_RP _ _ _ _ _ _ _ -> t_RP}

coq_T_RCD :: Bank_configuration -> Prelude.Int
coq_T_RCD bank_configuration =
  case bank_configuration of {
   Build_Bank_configuration _ _ _ _ _ _ _ t_RCD _ _ _ _ _ _ -> t_RCD}

type Bank_t = Prelude.Int

