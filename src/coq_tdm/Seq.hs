{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module Seq where

import qualified Prelude
import qualified Bool
import qualified Datatypes
import qualified Logic
import qualified Eqtype
import qualified Ssrbool
import qualified Ssreflect

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

__ :: any
__ = Prelude.error "Logical or arity value used"

cat :: (Datatypes.Coq_list a1) -> (Datatypes.Coq_list a1) ->
       Datatypes.Coq_list a1
cat s1 s2 =
  case s1 of {
   Datatypes.Coq_nil -> s2;
   Datatypes.Coq_cons x s1' -> Datatypes.Coq_cons x (cat s1' s2)}

filter :: (Ssrbool.Coq_pred a1) -> (Datatypes.Coq_list a1) ->
          Datatypes.Coq_list a1
filter a s =
  case s of {
   Datatypes.Coq_nil -> Datatypes.Coq_nil;
   Datatypes.Coq_cons x s' ->
    case a x of {
     Datatypes.Coq_true -> Datatypes.Coq_cons x (filter a s');
     Datatypes.Coq_false -> filter a s'}}

eqseq :: Eqtype.Equality__Coq_type -> (Datatypes.Coq_list
         Eqtype.Equality__Coq_sort) -> (Datatypes.Coq_list
         Eqtype.Equality__Coq_sort) -> Datatypes.Coq_bool
eqseq t s1 s2 =
  case s1 of {
   Datatypes.Coq_nil ->
    case s2 of {
     Datatypes.Coq_nil -> Datatypes.Coq_true;
     Datatypes.Coq_cons _ _ -> Datatypes.Coq_false};
   Datatypes.Coq_cons x1 s1' ->
    case s2 of {
     Datatypes.Coq_nil -> Datatypes.Coq_false;
     Datatypes.Coq_cons x2 s2' ->
      Datatypes.andb (Eqtype.eq_op t x1 x2) (eqseq t s1' s2')}}

eqseqP :: Eqtype.Equality__Coq_type -> Eqtype.Equality__Coq_axiom
          (Datatypes.Coq_list Eqtype.Equality__Coq_sort)
eqseqP t _top_assumption_ =
  let {
   _evar_0_ = \__top_assumption_ ->
    let {_evar_0_ = Bool.ReflectT} in
    let {_evar_0_0 = \_ _ -> Bool.ReflectF} in
    case __top_assumption_ of {
     Datatypes.Coq_nil -> _evar_0_;
     Datatypes.Coq_cons x x0 -> _evar_0_0 x x0}}
  in
  let {
   _evar_0_0 = \x1 s1 iHs __top_assumption_ ->
    let {_evar_0_0 = Bool.ReflectF} in
    let {
     _evar_0_1 = \x2 s2 ->
      Ssreflect.ssr_have (Eqtype.eqP t x1 x2) (\__top_assumption_0 ->
        let {
         _evar_0_1 = \_ ->
          let {_evar_0_1 = Ssrbool.iffP (eqseq t s1 s2) (iHs s2)} in
          Logic.eq_rect x1 _evar_0_1 x2}
        in
        let {_evar_0_2 = \_ -> Bool.ReflectF} in
        case __top_assumption_0 of {
         Bool.ReflectT -> _evar_0_1 __;
         Bool.ReflectF -> _evar_0_2 __})}
    in
    case __top_assumption_ of {
     Datatypes.Coq_nil -> _evar_0_0;
     Datatypes.Coq_cons x x0 -> _evar_0_1 x x0}}
  in
  Datatypes.list_rect _evar_0_ _evar_0_0 _top_assumption_

seq_eqMixin :: Eqtype.Equality__Coq_type -> Eqtype.Equality__Coq_mixin_of
               (Datatypes.Coq_list Eqtype.Equality__Coq_sort)
seq_eqMixin t =
  Eqtype.Equality__Mixin (eqseq t) (eqseqP t)

seq_eqType :: Eqtype.Equality__Coq_type -> Eqtype.Equality__Coq_type
seq_eqType t =
  unsafeCoerce seq_eqMixin t

rem :: Eqtype.Equality__Coq_type -> Eqtype.Equality__Coq_sort ->
       (Datatypes.Coq_list Eqtype.Equality__Coq_sort) -> Datatypes.Coq_list
       Eqtype.Equality__Coq_sort
rem t x s =
  case s of {
   Datatypes.Coq_nil -> s;
   Datatypes.Coq_cons y t0 ->
    case Eqtype.eq_op t y x of {
     Datatypes.Coq_true -> t0;
     Datatypes.Coq_false -> Datatypes.Coq_cons y (rem t x t0)}}

