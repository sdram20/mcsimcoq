{-# OPTIONS_GHC -cpp -XMagicHash #-}
{- For Hugs, use the option -F"cpp -P -traditional" -}

module TDM where

import qualified Prelude
import qualified Arbiter
import qualified Bank
import qualified Bool
import qualified Commands
import qualified Datatypes
import qualified Nat
import qualified Requests
import qualified Trace
import qualified Eqtype
import qualified Fintype
import qualified Seq
import qualified Ssrnat

#ifdef __GLASGOW_HASKELL__
import qualified GHC.Base
#else
-- HUGS
import qualified IOExts
#endif

#ifdef __GLASGOW_HASKELL__
unsafeCoerce :: a -> b
unsafeCoerce = GHC.Base.unsafeCoerce#
#else
-- HUGS
unsafeCoerce :: a -> b
unsafeCoerce = IOExts.unsafeCoerce
#endif

coq_ACT_date :: Bank.Bank_configuration -> Prelude.Int
coq_ACT_date bANK_CFG =
  Prelude.succ (Bank.coq_T_RP bANK_CFG)

coq_CAS_date :: Bank.Bank_configuration -> Prelude.Int
coq_CAS_date bANK_CFG =
  Ssrnat.addn (coq_ACT_date bANK_CFG) (Prelude.succ
    (Bank.coq_T_RCD bANK_CFG))

data TDM_configuration =
   Build_TDM_configuration Prelude.Int Prelude.Int

coq_SL :: Bank.Bank_configuration -> TDM_configuration -> Prelude.Int
coq_SL _ tDM_configuration =
  case tDM_configuration of {
   Build_TDM_configuration sL _ -> sL}

coq_SN :: Bank.Bank_configuration -> TDM_configuration -> Prelude.Int
coq_SN _ tDM_configuration =
  case tDM_configuration of {
   Build_TDM_configuration _ sN -> sN}

coq_OZCycle :: Bank.Bank_configuration -> TDM_configuration ->
               Fintype.Coq_ordinal
coq_OZCycle _ _ =
  0

coq_OACT_date :: Bank.Bank_configuration -> TDM_configuration ->
                 Fintype.Coq_ordinal
coq_OACT_date bANK_CFG _ =
  coq_ACT_date bANK_CFG

coq_OCAS_date :: Bank.Bank_configuration -> TDM_configuration ->
                 Fintype.Coq_ordinal
coq_OCAS_date bANK_CFG _ =
  coq_CAS_date bANK_CFG

coq_OLastCycle :: Bank.Bank_configuration -> TDM_configuration ->
                  Fintype.Coq_ordinal
coq_OLastCycle bANK_CFG tDM_CFG =
  Nat.pred (coq_SL bANK_CFG tDM_CFG)

coq_OZSlot :: Bank.Bank_configuration -> TDM_configuration ->
              Fintype.Coq_ordinal
coq_OZSlot _ _ =
  0

coq_Slot_t :: Bank.Bank_configuration -> TDM_configuration ->
              Eqtype.Equality__Coq_type
coq_Slot_t bANK_CFG tDM_CFG =
  Fintype.ordinal_eqType (coq_SN bANK_CFG tDM_CFG)

coq_REQESTOR_CFG :: Bank.Bank_configuration -> TDM_configuration ->
                    Bank.Requestor_configuration
coq_REQESTOR_CFG =
  coq_Slot_t

type Counter_t = Fintype.Coq_ordinal

data TDM_state_t =
   IDLE Eqtype.Equality__Coq_sort Counter_t Requests.Requests_t
 | RUNNING Eqtype.Equality__Coq_sort Counter_t Requests.Requests_t Requests.Request_t

coq_TDM_state_eqdef :: Bank.Bank_configuration -> TDM_configuration ->
                       TDM_state_t -> TDM_state_t -> Datatypes.Coq_bool
coq_TDM_state_eqdef bANK_CFG tDM_CFG a b =
  case a of {
   IDLE sa ca pa ->
    case b of {
     IDLE sb cb pb ->
      Datatypes.andb
        (Datatypes.andb (Eqtype.eq_op (coq_Slot_t bANK_CFG tDM_CFG) sa sb)
          (Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
            (unsafeCoerce ca) (unsafeCoerce cb)))
        (Eqtype.eq_op
          (Seq.seq_eqType
            (Requests.coq_Request_eqType (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
              bANK_CFG)) (unsafeCoerce pa) (unsafeCoerce pb));
     RUNNING _ _ _ _ -> Datatypes.Coq_false};
   RUNNING sa ca pa ra ->
    case b of {
     IDLE _ _ _ -> Datatypes.Coq_false;
     RUNNING sb cb pb rb ->
      Datatypes.andb
        (Datatypes.andb
          (Datatypes.andb (Eqtype.eq_op (coq_Slot_t bANK_CFG tDM_CFG) sa sb)
            (Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
              (unsafeCoerce ca) (unsafeCoerce cb)))
          (Eqtype.eq_op
            (Seq.seq_eqType
              (Requests.coq_Request_eqType
                (coq_REQESTOR_CFG bANK_CFG tDM_CFG) bANK_CFG))
            (unsafeCoerce pa) (unsafeCoerce pb)))
        (Eqtype.eq_op
          (Requests.coq_Request_eqType (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
            bANK_CFG) (unsafeCoerce ra) (unsafeCoerce rb))}}

coq_TDM_state_eqn :: Bank.Bank_configuration -> TDM_configuration ->
                     Eqtype.Equality__Coq_axiom TDM_state_t
coq_TDM_state_eqn bANK_CFG tDM_CFG x y =
  let {b = coq_TDM_state_eqdef bANK_CFG tDM_CFG x y} in
  case b of {
   Datatypes.Coq_true -> Bool.ReflectT;
   Datatypes.Coq_false -> Bool.ReflectF}

coq_TDM_state_eqMixin :: Bank.Bank_configuration -> TDM_configuration ->
                         Eqtype.Equality__Coq_mixin_of TDM_state_t
coq_TDM_state_eqMixin bANK_CFG tDM_CFG =
  Eqtype.Equality__Mixin (coq_TDM_state_eqdef bANK_CFG tDM_CFG)
    (coq_TDM_state_eqn bANK_CFG tDM_CFG)

coq_TDM_state_eqType :: Bank.Bank_configuration -> TDM_configuration ->
                        Eqtype.Equality__Coq_type
coq_TDM_state_eqType bANK_CFG tDM_CFG =
  unsafeCoerce coq_TDM_state_eqMixin bANK_CFG tDM_CFG

coq_ARBITER_CFG :: Bank.Bank_configuration -> TDM_configuration ->
                   Bank.Arbiter_configuration
coq_ARBITER_CFG _ _ =
  Bank.Build_Arbiter_configuration

coq_Next_cycle :: Bank.Bank_configuration -> TDM_configuration -> Counter_t
                  -> Counter_t
coq_Next_cycle bANK_CFG tDM_CFG c =
  let {
   nextc = Ssrnat.leq (Prelude.succ (Prelude.succ
             (Fintype.nat_of_ord (coq_SL bANK_CFG tDM_CFG) c)))
             (coq_SL bANK_CFG tDM_CFG)}
  in
  case nextc of {
   Datatypes.Coq_true -> Prelude.succ
    (Fintype.nat_of_ord (coq_SL bANK_CFG tDM_CFG) c);
   Datatypes.Coq_false -> coq_OZCycle bANK_CFG tDM_CFG}

coq_Next_slot :: Bank.Bank_configuration -> TDM_configuration ->
                 Eqtype.Equality__Coq_sort -> Counter_t ->
                 Eqtype.Equality__Coq_sort
coq_Next_slot bANK_CFG tDM_CFG s c =
  case Ssrnat.leq (Prelude.succ (Prelude.succ
         (Fintype.nat_of_ord (coq_SL bANK_CFG tDM_CFG) c)))
         (coq_SL bANK_CFG tDM_CFG) of {
   Datatypes.Coq_true -> s;
   Datatypes.Coq_false ->
    let {
     nexts = Ssrnat.leq (Prelude.succ (Prelude.succ
               (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG)
                 (unsafeCoerce s)))) (coq_SN bANK_CFG tDM_CFG)}
    in
    case nexts of {
     Datatypes.Coq_true ->
      unsafeCoerce (Prelude.succ
        (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG) (unsafeCoerce s)));
     Datatypes.Coq_false -> unsafeCoerce coq_OZSlot bANK_CFG tDM_CFG}}

coq_Enqueue :: Bank.Bank_configuration -> TDM_configuration ->
               Requests.Requests_t -> Requests.Requests_t ->
               Datatypes.Coq_list Requests.Request_t
coq_Enqueue _ _ r p =
  Seq.cat p r

coq_Dequeue :: Bank.Bank_configuration -> TDM_configuration ->
               Eqtype.Equality__Coq_sort -> Requests.Requests_t ->
               Datatypes.Coq_list Eqtype.Equality__Coq_sort
coq_Dequeue bANK_CFG tDM_CFG r p =
  Seq.rem
    (Requests.coq_Request_eqType (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
      bANK_CFG) r (unsafeCoerce p)

coq_Pending_of :: Bank.Bank_configuration -> TDM_configuration ->
                  Eqtype.Equality__Coq_sort -> Requests.Requests_t ->
                  Datatypes.Coq_list Requests.Request_t
coq_Pending_of bANK_CFG tDM_CFG s p =
  Seq.filter (\r ->
    Eqtype.eq_op (Bank.coq_Requestor_t (coq_REQESTOR_CFG bANK_CFG tDM_CFG))
      (Requests.coq_Requestor (coq_REQESTOR_CFG bANK_CFG tDM_CFG) bANK_CFG r)
      s) p

coq_Init_state :: Bank.Bank_configuration -> TDM_configuration ->
                  Requests.Requests_t -> Arbiter.Arbiter_state_t
coq_Init_state bANK_CFG tDM_CFG r =
  Arbiter.Coq_mkArbiterState Datatypes.Coq_nil 0
    (unsafeCoerce (IDLE (unsafeCoerce coq_OZSlot bANK_CFG tDM_CFG)
      (coq_OZCycle bANK_CFG tDM_CFG)
      (coq_Enqueue bANK_CFG tDM_CFG r Datatypes.Coq_nil)))

coq_ACommand_t :: Bank.Bank_configuration -> TDM_configuration ->
                  Commands.Command_t
coq_ACommand_t =
  Prelude.error "AXIOM TO BE REALIZED"

coq_Next_state :: Bank.Bank_configuration -> TDM_configuration ->
                  Requests.Requests_t -> Arbiter.Arbiter_state_t ->
                  Arbiter.Arbiter_state_t
coq_Next_state bANK_CFG tDM_CFG r aS =
  let {
   cmds = Arbiter.coq_Arbiter_Commands (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
            (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS}
  in
  let {
   t = Prelude.succ
    (Arbiter.coq_Arbiter_Time (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
      (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS)}
  in
  let {
   filtered_var = Arbiter.coq_Arbiter_State
                    (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
                    (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS}
  in
  case unsafeCoerce filtered_var of {
   IDLE s c p ->
    let {p' = coq_Enqueue bANK_CFG tDM_CFG r p} in
    let {s' = coq_Next_slot bANK_CFG tDM_CFG s c} in
    let {c' = coq_Next_cycle bANK_CFG tDM_CFG c} in
    case Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
           (unsafeCoerce c) (unsafeCoerce coq_OZCycle bANK_CFG tDM_CFG) of {
     Datatypes.Coq_true ->
      let {filtered_var0 = coq_Pending_of bANK_CFG tDM_CFG s p} in
      case filtered_var0 of {
       Datatypes.Coq_nil -> Arbiter.Coq_mkArbiterState cmds t
        (unsafeCoerce (IDLE s' c' p'));
       Datatypes.Coq_cons r0 _ ->
        let {
         cmds' = Datatypes.Coq_cons
          (Commands.coq_PRE_of_req (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
            bANK_CFG r0 t) cmds}
        in
        Arbiter.Coq_mkArbiterState cmds' t
        (unsafeCoerce (RUNNING s' c'
          (coq_Enqueue bANK_CFG tDM_CFG r
            (unsafeCoerce coq_Dequeue bANK_CFG tDM_CFG r0 p)) r0))};
     Datatypes.Coq_false -> Arbiter.Coq_mkArbiterState cmds t
      (unsafeCoerce (IDLE s' c' p'))};
   RUNNING s c p r0 ->
    let {p' = coq_Enqueue bANK_CFG tDM_CFG r p} in
    let {s' = coq_Next_slot bANK_CFG tDM_CFG s c} in
    let {c' = coq_Next_cycle bANK_CFG tDM_CFG c} in
    case Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
           (unsafeCoerce c) (unsafeCoerce coq_OACT_date bANK_CFG tDM_CFG) of {
     Datatypes.Coq_true ->
      let {
       cmds' = Datatypes.Coq_cons
        (Commands.coq_ACT_of_req (coq_REQESTOR_CFG bANK_CFG tDM_CFG) bANK_CFG
          r0 t) cmds}
      in
      Arbiter.Coq_mkArbiterState cmds' t (unsafeCoerce (RUNNING s' c' p' r0));
     Datatypes.Coq_false ->
      case Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
             (unsafeCoerce c) (unsafeCoerce coq_OCAS_date bANK_CFG tDM_CFG) of {
       Datatypes.Coq_true ->
        let {
         cmds' = Datatypes.Coq_cons
          (Commands.coq_CAS_of_req (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
            bANK_CFG r0 t) cmds}
        in
        Arbiter.Coq_mkArbiterState cmds' t
        (unsafeCoerce (RUNNING s' c' p' r0));
       Datatypes.Coq_false ->
        case Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
               (unsafeCoerce c)
               (unsafeCoerce coq_OLastCycle bANK_CFG tDM_CFG) of {
         Datatypes.Coq_true -> Arbiter.Coq_mkArbiterState cmds t
          (unsafeCoerce (IDLE s' c' p'));
         Datatypes.Coq_false -> Arbiter.Coq_mkArbiterState cmds t
          (unsafeCoerce (RUNNING s' c' p' r0))}}}}

coq_TDM_counter :: Bank.Bank_configuration -> TDM_configuration ->
                   Arbiter.Arbiter_state_t -> Counter_t
coq_TDM_counter bANK_CFG tDM_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State
         (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
         (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS of {
   IDLE _ c _ -> c;
   RUNNING _ c _ _ -> c}

coq_TDM_pending :: Bank.Bank_configuration -> TDM_configuration ->
                   Arbiter.Arbiter_state_t -> Requests.Requests_t
coq_TDM_pending bANK_CFG tDM_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State
         (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
         (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS of {
   IDLE _ _ p -> p;
   RUNNING _ _ p _ -> p}

coq_TDM_request :: Bank.Bank_configuration -> TDM_configuration ->
                   Arbiter.Arbiter_state_t -> Datatypes.Coq_option
                   Requests.Request_t
coq_TDM_request bANK_CFG tDM_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State
         (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
         (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS of {
   IDLE _ _ _ -> Datatypes.None;
   RUNNING _ _ _ r -> Datatypes.Some r}

coq_TDM_slot :: Bank.Bank_configuration -> TDM_configuration ->
                Arbiter.Arbiter_state_t -> Eqtype.Equality__Coq_sort
coq_TDM_slot bANK_CFG tDM_CFG aS =
  case unsafeCoerce Arbiter.coq_Arbiter_State
         (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
         (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aS of {
   IDLE s _ _ -> s;
   RUNNING s _ _ _ -> s}

coq_TDM_arbiter_trace :: Bank.Bank_configuration -> TDM_configuration ->
                         Arbiter.Implementation_t
coq_TDM_arbiter_trace bANK_CFG tDM_CFG =
  Arbiter.Coq_mkImplementation (coq_Init_state bANK_CFG tDM_CFG)
    (coq_Next_state bANK_CFG tDM_CFG)

coq_Last_slot_start :: Bank.Bank_configuration -> TDM_configuration ->
                       Arbiter.Arrival_function_t -> Prelude.Int ->
                       Prelude.Int
coq_Last_slot_start bANK_CFG tDM_CFG aF ta =
  Ssrnat.subn ta
    (Fintype.nat_of_ord (coq_SL bANK_CFG tDM_CFG)
      (coq_TDM_counter bANK_CFG tDM_CFG
        (Arbiter.coq_Default_arbitrate (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
          (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aF
          (coq_TDM_arbiter_trace bANK_CFG tDM_CFG) ta)))

coq_Next_slot_start :: Bank.Bank_configuration -> TDM_configuration ->
                       Arbiter.Arrival_function_t -> Prelude.Int ->
                       Prelude.Int
coq_Next_slot_start bANK_CFG tDM_CFG aF ta =
  Ssrnat.addn (coq_Last_slot_start bANK_CFG tDM_CFG aF ta)
    (coq_SL bANK_CFG tDM_CFG)

coq_Closest_slot_start :: Bank.Bank_configuration -> TDM_configuration ->
                          Arbiter.Arrival_function_t -> Prelude.Int ->
                          Prelude.Int
coq_Closest_slot_start bANK_CFG tDM_CFG aF ta =
  case Eqtype.eq_op (Fintype.ordinal_eqType (coq_SL bANK_CFG tDM_CFG))
         (unsafeCoerce coq_TDM_counter bANK_CFG tDM_CFG
           (Arbiter.coq_Default_arbitrate (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
             (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aF
             (coq_TDM_arbiter_trace bANK_CFG tDM_CFG) ta))
         (unsafeCoerce coq_OZCycle bANK_CFG tDM_CFG) of {
   Datatypes.Coq_true -> ta;
   Datatypes.Coq_false -> coq_Next_slot_start bANK_CFG tDM_CFG aF ta}

coq_Requestor_slot_start :: Bank.Bank_configuration -> TDM_configuration ->
                            Arbiter.Arrival_function_t -> Prelude.Int ->
                            Eqtype.Equality__Coq_sort -> Prelude.Int
coq_Requestor_slot_start bANK_CFG tDM_CFG aF ta s =
  let {aligned_ta = coq_Closest_slot_start bANK_CFG tDM_CFG aF ta} in
  let {
   current_slot = coq_TDM_slot bANK_CFG tDM_CFG
                    (Arbiter.coq_Default_arbitrate
                      (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
                      (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aF
                      (coq_TDM_arbiter_trace bANK_CFG tDM_CFG) aligned_ta)}
  in
  case Ssrnat.leq
         (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG)
           (unsafeCoerce current_slot))
         (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG) (unsafeCoerce s)) of {
   Datatypes.Coq_true ->
    Ssrnat.addn aligned_ta
      (Ssrnat.muln
        (Ssrnat.subn
          (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG) (unsafeCoerce s))
          (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG)
            (unsafeCoerce current_slot))) (coq_SL bANK_CFG tDM_CFG));
   Datatypes.Coq_false ->
    Ssrnat.addn aligned_ta
      (Ssrnat.muln
        (Ssrnat.addn
          (Ssrnat.subn (coq_SN bANK_CFG tDM_CFG)
            (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG)
              (unsafeCoerce current_slot)))
          (Fintype.nat_of_ord (coq_SN bANK_CFG tDM_CFG) (unsafeCoerce s)))
        (coq_SL bANK_CFG tDM_CFG))}

coq_TDM_arbitrate :: Bank.Bank_configuration -> TDM_configuration ->
                     Arbiter.Arrival_function_t -> Prelude.Int ->
                     Trace.Trace_t
coq_TDM_arbitrate bANK_CFG tDM_CFG aF t =
  Trace.Coq_mkTrace
    (Arbiter.coq_Arbiter_Commands (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
      (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG
      (Arbiter.coq_Default_arbitrate (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
        (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aF
        (coq_TDM_arbiter_trace bANK_CFG tDM_CFG) t))
    (Arbiter.coq_Arbiter_Time (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
      (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG
      (Arbiter.coq_Default_arbitrate (coq_REQESTOR_CFG bANK_CFG tDM_CFG)
        (coq_ARBITER_CFG bANK_CFG tDM_CFG) bANK_CFG aF
        (coq_TDM_arbiter_trace bANK_CFG tDM_CFG) t))

coq_TDM_arbiter :: Bank.Bank_configuration -> TDM_configuration ->
                   Arbiter.Arrival_function_t -> Arbiter.Arbiter_t
coq_TDM_arbiter =
  coq_TDM_arbitrate

coq_BANK_CFG :: Bank.Bank_configuration
coq_BANK_CFG =
  Bank.Build_Bank_configuration (Prelude.succ 0) (Prelude.succ 0)
    (Prelude.succ 0) (Prelude.succ (Prelude.succ (Prelude.succ 0)))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    0)))))))))))))))))))) (Prelude.succ (Prelude.succ (Prelude.succ 0)))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ 0))))))) (Prelude.succ (Prelude.succ 0))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ 0))))
    (Prelude.succ (Prelude.succ 0)) (Prelude.succ 0) (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ 0))))))))))
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    0)))))))))) (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ 0))))))))))))

coq_TDM_CFG :: TDM_configuration
coq_TDM_CFG =
  Build_TDM_configuration (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ (Prelude.succ
    (Prelude.succ (Prelude.succ
    0))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
    (Prelude.succ (Prelude.succ (Prelude.succ 0)))

coq_Req1 :: Requests.Request_t
coq_Req1 =
  Requests.Coq_mkReq (unsafeCoerce 0) 0 Requests.RD 0 (Prelude.succ 0)

coq_Req2 :: Requests.Request_t
coq_Req2 =
  Requests.Coq_mkReq (unsafeCoerce (Prelude.succ 0)) (Prelude.succ
    (Prelude.succ (Prelude.succ (Prelude.succ 0)))) Requests.RD 0
    (Prelude.succ 0)

coq_Input :: Datatypes.Coq_list Requests.Request_t
coq_Input =
  Datatypes.Coq_cons coq_Req2 (Datatypes.Coq_cons coq_Req1 Datatypes.Coq_nil)

coq_AF :: Arbiter.Arrival_function_t
coq_AF =
  Arbiter.coq_Default_arrival_function_t
    (coq_REQESTOR_CFG coq_BANK_CFG coq_TDM_CFG) coq_BANK_CFG coq_Input

