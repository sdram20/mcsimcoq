module Ssreflect where

import qualified Prelude

ssr_have :: a1 -> (a1 -> a2) -> a2
ssr_have step rest =
  rest step

