module Nat where

import qualified Prelude

pred :: Prelude.Int -> Prelude.Int
pred = (\n -> Prelude.max 0 (Prelude.pred n))

sub :: Prelude.Int -> Prelude.Int -> Prelude.Int
sub = (\n m -> Prelude.max 0 (n Prelude.- m))

