module ForeignCommand where

import Foreign
import Foreign.StablePtr
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types

#include "ForeignCommand_t.h"

data ForeignCommand_t = ForeignCommand_t {
    busPacketType :: CUInt,
    requestorID :: CUInt,
    address :: CUInt,
    column :: CUInt,
    row :: CUInt,
    subArray :: CUInt,
    bank :: CUInt,
    bankGroup :: CUInt,
    rank :: CUInt,
    ddata :: StablePtr CInt,
    arriveTime :: CUInt,
    -- had to convert this to int instead of bool
    postCommand :: CBool,
    postCounter :: CUInt,
    addressMap :: CIntPtr
} deriving (Eq)

type PtrCommand = Ptr ForeignCommand_t

instance Storable ForeignCommand_t where
    sizeOf    _ = (#size ForeignCommand_t)
    alignment _ = alignment (undefined :: CInt)
    peek ptr = do
        r_busPacketType <- (#peek ForeignCommand_t, busPacketType) ptr
        r_requestorID <- (#peek ForeignCommand_t, requestorID) ptr
        r_address <- (#peek ForeignCommand_t, address) ptr
        r_col <- (#peek ForeignCommand_t, column) ptr
        r_row <- (#peek ForeignCommand_t, row) ptr
        r_subArray <- (#peek ForeignCommand_t, subArray) ptr
        r_bank <- (#peek ForeignCommand_t, bank) ptr
        r_bankGroup <- (#peek ForeignCommand_t, bankGroup) ptr
        r_rank <- (#peek ForeignCommand_t, rank) ptr
        r_ddata <- (#peek ForeignCommand_t, ddata) ptr
        r_arriveTime <- (#peek ForeignCommand_t, arriveTime) ptr
        r_postCommand <- (#peek ForeignCommand_t, postCommand) ptr
        r_postCounter <- (#peek ForeignCommand_t, postCounter) ptr
        r_addressMap <- (#peek ForeignCommand_t, addressMap) ptr
        return ForeignCommand_t {
            busPacketType = r_busPacketType,
            requestorID = r_requestorID,
            address = r_address,
            column = r_col,
            row = r_row,
            subArray = r_subArray,
            bank = r_bank,
            bankGroup = r_bankGroup,
            rank = r_rank,
            ddata = r_ddata,
            arriveTime = r_arriveTime,
            postCommand = r_postCommand,
            postCounter = r_postCounter,
            addressMap = r_addressMap
        }
    poke ptr (ForeignCommand_t busPacketType requestorID address column row subArray bank bankGroup rank ddata arriveTime postCommand postCounter addressMap) = do
        (#poke ForeignCommand_t, busPacketType) ptr busPacketType
        (#poke ForeignCommand_t, requestorID) ptr requestorID
        (#poke ForeignCommand_t, address) ptr address
        (#poke ForeignCommand_t, column) ptr column
        (#poke ForeignCommand_t, row) ptr row
        (#poke ForeignCommand_t, subArray) ptr subArray
        (#poke ForeignCommand_t, bank) ptr bank
        (#poke ForeignCommand_t, bankGroup) ptr bankGroup
        (#poke ForeignCommand_t, rank) ptr rank
        (#poke ForeignCommand_t, ddata) ptr ddata
        (#poke ForeignCommand_t, arriveTime) ptr arriveTime
        (#poke ForeignCommand_t, postCommand) ptr postCommand
        (#poke ForeignCommand_t, postCounter) ptr postCounter
        (#poke ForeignCommand_t, addressMap) ptr addressMap