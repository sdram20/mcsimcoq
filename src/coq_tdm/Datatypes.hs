module Datatypes where

import qualified Prelude

data Coq_bool =
   Coq_true
 | Coq_false

andb :: Coq_bool -> Coq_bool -> Coq_bool
andb b1 b2 =
  case b1 of {
   Coq_true -> b2;
   Coq_false -> Coq_false}

data Coq_option a =
   Some a
 | None

data Coq_list a =
   Coq_nil
 | Coq_cons a (Coq_list a)

list_rect :: a2 -> (a1 -> (Coq_list a1) -> a2 -> a2) -> (Coq_list a1) -> a2
list_rect f f0 l =
  case l of {
   Coq_nil -> f;
   Coq_cons y l0 -> f0 y l0 (list_rect f f0 l0)}

