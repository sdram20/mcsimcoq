module Trace where

import qualified Prelude
import qualified Commands

data Trace_t =
   Coq_mkTrace Commands.Commands_t Prelude.Int

