# MCsimCoq

This is a modified version of https://github.com/uwuser/MCsim. We use Coq generated code, stored in `src/coq_fifo` and `src/coq_tdm`, to run MCsim. Currently, the contents of these folders are not synchronized with the framework, i.e., the code there is static and is not regenerated. This is because we have updated our framework and re-working the integration with MCsim is a work-in-progress. 

In order to run MCsim using the Coq generated code, follow the steps below:

1. Go to src/coq_fifo and src/coq_tdm and do
```
make main
```
This will build the objects from the haskell sources and generate the needed headers for C++

2. Do `make` on src/ to build MCsim

3. The standard version works with Coq's TDM. If you want to change to FIFO, perform the following steps:
- Change the calls to `tdm_init_state_wrapper` and `tdm_next_state_wrapper` to `fifo_init_state_wrapper` and `fifo_next_state_wrapper` in `src/MemoryController.cpp` respectively.
- Change the `#includes` in `src/MemoryController.h` to include the files on `coq_fifo/` instead of `coq_tdm/`
- Change the following line on `src/makefile`
```
SRC = $(wildcard ../dram/*.cpp) $(wildcard *.cpp) $(wildcard coq_tdm/*.hs)
```
to
```
SRC = $(wildcard ../dram/*.cpp) $(wildcard *.cpp) $(wildcard coq_fifo/*.hs)
```

4. From `src/`, run MCSim by doing

```
./MCsim -t Mem_Trace/1M_rand.trc -s ../system/Round/Round.ini -C 1 -R 1 -G DDR3 -D 1600H -S 2Gb_x8 -n 4 -c 15
```
The options are explained in the original repository

For the Coq integration to work properly, specify `-s` to be `../system/Round/Round.ini` (this is a workaround)

## General workings of the Coq-MCsim integration

A couple of things are hard-coded in `src/coq_fifo/App.hs` and `src/coq_tdm/App.hs`. One needs to manually change the bank configuration with the right constraints for the simulated device and number of banks.
```
let bkcfg = Bank.Build_Bank_configuration 4 4 10 7 38 44 14 14 36 8 16 10 8 4
```
Standard values are for device DDR3-2133N

The value of the SL and WAIT parameters are also hard-coded in `App.hs`
```
let fifocfg = 10
```

### FIFO specific and expected behaviour 

- WAIT values smaller then T_RP + T_RCD will stall the simulation, as the Coq controller will try to output commands for requestor without the previous ones having finished. For DDR3 2133N, this means WAIT < 28.

- WAIT = T_RCD + T_RC will break simulation (?)

- WAIT > T_RCD + T_RC will run simulation normally. 

Although the theoretical minimal WAIT is given by: T_RCD + T_RC + T_WL + T_BURST + T_WR = 14 + 14 + 10 + 4 + 16 = 58 (on DDR3 2133N), MCsim does not detect a violation because of the current configuration. Since requestors are treated fairly and banks are privatized, no consecutive requests target the same bank, and therefore, the (T_WL + T_BURST + T_WR) part of the constraint can be ignored. Therefore, for WAIT = 29, for instance, the Coq FIFO controller will output a write CAS at clock cycle 28 followed by a PRE command on clock cycle 29.